## Journalctl Desktop Notification

The notification notifies on the desktop when any error message appeared in Journalctl log.

### Requirements
* Install `dunst`
* Install `systemd` due to `journalctl`
* Any linux

### Configs

Configuration is located in `/etc/journalctl-desktop-notification.conf`

* Exclusive annoying spam errors
```
ERROR_FILTER="Spam1|Spam2|Spam3"
```

* Time frequency (secound) of the check journalctl
```
TIME_INTERVAL="2"
```

* Custom terminal app will be opened

Example: konsole for KDE terminal
```
TERMINAL="konsole"
```

* Custom icon of error message
```
ERROR_ICON="system-error"
```



### Autostart

How to enable it for autostart:
```
cp /usr/share/applications/journalctl-desktop-notification.desktop ~/.config/autostart/
```

### Troubleshooting

KDE notification is replaced with “knopwob dunst” or an ugly notification after some update.
How to switch back the default notification for KDE:
```
mkdir -p ~/.local/share/dbus-1/services/
ln -s /usr/share/dbus-1/services/org.kde.plasma.Notifications.service \
  ~/.local/share/dbus-1/services/org.kde.plasma.Notifications.service
```
